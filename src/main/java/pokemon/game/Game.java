package pokemon.game;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import pokemon.io.TriviaIO;
import pokemon.io.UserSessionIO;
import pokemon.io.impl.FileTriviaIO;
import pokemon.io.impl.FileUserSessionIO;
import pokemon.model.Pokemon;
import pokemon.model.Question;
import pokemon.model.UserSession;
import pokemon.model.exception.PokemonIOException;
import pokemon.util.PokemonUtils;

public class Game {

	private static Map<String, Set<Pokemon>> pokemonEnvironment;
	private static UserSession userSession;
	private static Question[] questions;
	private static String location;
	private static Pokemon pokemon;
	private static Scanner scanner;

	private static final TriviaIO triviaIO = new FileTriviaIO();
	private static final UserSessionIO userSessionIO = new FileUserSessionIO();

	public static void main(String[] args) {
		pokemonEnvironment = PokemonUtils.initPokemonGame();

		UserSessionIO userSessionIO = new FileUserSessionIO();
		try {
			userSession = userSessionIO.getPreviousUserSession();
		} catch (PokemonIOException pokemonIOException) {
			System.out.println(pokemonIOException.getMessage() + "\n");
			pokemonIOException.printStackTrace();
			userSession = new UserSession();
		}

		try {
			questions = triviaIO.fetchQuestions();
		} catch (PokemonIOException pokemonReadingException) {
			System.out.println(pokemonReadingException.getMessage()+ "\n");
			pokemonReadingException.printStackTrace();
			questions = null;
		}

		scanner = new Scanner(System.in);
		mainMenu();
	}

	private static void mainMenu() {
		System.out.println("Choose your destiny:");
		System.out.println("-------------------------\n");
		System.out.println("1 - Catch them all");
		System.out.println("2 - Win pokeballs");
		System.out.println("3 - Inventory");
		System.out.println("4 - Quit");

		int choice = scanner.nextInt();

		switch (choice) {
		case 1:
			generateRandomGame();
			catchThemAllSubMenu();
			break;
		case 2:
			playTrivia();
			break;
		case 3:
			PokemonUtils.printInventory(userSession);
			mainMenu();
			break;
		case 4:
			saveSession();
			scanner.close();
			System.exit(0);
		default:
			System.out.println("Please select one of the available options");
			mainMenu();
		}
	}
	
	private static void generateRandomGame() {
		location = PokemonUtils.getRandomLocation(pokemonEnvironment);
		pokemon = PokemonUtils.getRandomPokemon(pokemonEnvironment.get(location));
	}
	

	private static void saveSession() {
		try {
			userSessionIO.saveUserSession(userSession);
		} catch (PokemonIOException pokemonIOException) {
			System.out.println(pokemonIOException.getMessage());
			mainMenu();
		}
	}


	private static void catchThemAllSubMenu() {
		System.out.println(PokemonUtils.printCurrentSituation(pokemon, location));
		System.out.println("\n Catch them all: ");
		System.out.println("-------------------------\n");
		System.out.println("1 - Catch");
		System.out.println("2 - Back");

		int choice = scanner.nextInt();

		switch (choice) {
		case 1:
			if (userSession.getNumberOfPokeBalls() <= 0) {
				System.out.println(
						"You ran out of pokeballs! You need to win some pokeballs before you can catch a pokemon!");
				mainMenu();
				break;
			}
			userSession.substractPokeBall();
			if (pokemon.isCaptured()) {
				userSession.getCatchedPokemons().add(pokemon);
				System.out.println("Congratulations,  " + pokemon.toString() + " has been caught!!!");
				PokemonUtils.printNoOfPokeBalls(userSession);
				mainMenu();
			} else {
				System.out.println("Unfortunately the pokemon got away. You can stil try to catch it though...");
				PokemonUtils.printNoOfPokeBalls(userSession);
				catchThemAllSubMenu();
			}
			break;
		case 2:
			mainMenu();
			break;
		default:
			System.out.println("Please select one of the available options");
			catchThemAllSubMenu();
		}
	}

	private static void playTrivia() {
		if (questions == null) {
			System.out.println("There are no questions available");
			mainMenu();
		}

		Question question = questions[PokemonUtils.randInt(questions.length)];

		System.out.println(question.getQuestion());
		String choice = "";
		while (choice.trim().equals("")) {
			choice = scanner.nextLine();
		}

		if (question.getAnswer().equalsIgnoreCase(choice.trim())) {
			userSession.addPokeBalls(question.getPokeBallsReward());
			System.out.println("Congrats! You won " + question.getPokeBallsReward() + " pokeBalls");
		} else {

			System.out.println("Sorry! Wrong Answer :()");
		}
		mainMenu();
	}
}
