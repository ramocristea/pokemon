package pokemon.action;


/**
 * Activated when catching a pokemon
 *
 */
public interface Catchable {
	/**
	 * 
	 * @return the rarity of the pokemon (the user's chance to catch the pokemon)
	 */
	public int catchMe();
}
