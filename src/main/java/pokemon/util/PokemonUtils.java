package pokemon.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import pokemon.model.FirePokemon;
import pokemon.model.Pokemon;
import pokemon.model.Rarity;
import pokemon.model.UserSession;
import pokemon.model.WaterPokemon;

public class PokemonUtils {
	
	public static final Integer MAX = 100;

	public static void printInventory(UserSession userSession) {
		System.out.println("You have " + userSession.getNumberOfPokeBalls() + " pokeBalls");
		System.out.println("You have caught " + userSession.getCatchedPokemons().size() + " pokemons:");
		for (Pokemon pokemon : userSession.getCatchedPokemons()) {
			System.out.println(pokemon);
		}
	}
	
	public static Map<String, Set<Pokemon>> initPokemonGame() {
		Map<String, Set<Pokemon>> environment = new HashMap<String, Set<Pokemon>>();
		Set<Pokemon> pataratPokemons = new HashSet<Pokemon>();
		pataratPokemons.add(new FirePokemon("Charizar", "burning red", Rarity.EPIC));
		pataratPokemons.add(new FirePokemon("Ponyta", "yellow", Rarity.MAGIC));
		environment.put("Patarat", pataratPokemons);
		Set<Pokemon> somesPokemons = new HashSet<Pokemon>();
		somesPokemons.add(new WaterPokemon("Seel", "light blue", Rarity.MAGIC));
		somesPokemons.add(new WaterPokemon("Squirtle", "light blue", Rarity.RARE));
		environment.put("Somes", somesPokemons);
		Set<Pokemon> piataUniriiPokemons = new HashSet<Pokemon>();
		piataUniriiPokemons.add(new WaterPokemon("Krabby", "orange", Rarity.LEGENDARY));
		piataUniriiPokemons.add(new FirePokemon("Charmander", "orange", Rarity.RARE));
		environment.put("Piata Unirii", piataUniriiPokemons);
		return environment;
	}

	public static String getRandomLocation(Map<String, Set<Pokemon>> pokemonEnvironment) {
		return new ArrayList<String>(pokemonEnvironment.keySet()).get(randInt(pokemonEnvironment.keySet().size()));
	}

	public static Pokemon getRandomPokemon(Set<Pokemon> pokemons) {
		return new ArrayList<Pokemon>(pokemons).get(randInt(pokemons.size()));
	}

	public static String printCurrentSituation(Pokemon pokemon, String location) {
		return "You arrived at " + location + " and you see " + pokemon.toString();
	}

	public static int randInt(int max) {
		return new Random().nextInt(max);
	}

	public static void printNoOfPokeBalls(UserSession userSession) {
		System.out.println("You now have " + userSession.getNumberOfPokeBalls() + " pokeballs");
	}
	
}
