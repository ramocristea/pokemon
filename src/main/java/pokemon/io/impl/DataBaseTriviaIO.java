package pokemon.io.impl;

import pokemon.io.TriviaIO;
import pokemon.model.Question;
import pokemon.model.exception.PokemonIOException;

public class DataBaseTriviaIO implements TriviaIO {

	@Override
	public Question[] fetchQuestions() throws PokemonIOException {
		throw new UnsupportedOperationException("Reminder: Implement the question reading from database");
	}

}
