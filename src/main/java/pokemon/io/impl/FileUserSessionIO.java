package pokemon.io.impl;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import pokemon.io.UserSessionIO;
import pokemon.model.UserSession;
import pokemon.model.exception.PokemonIOException;

public class FileUserSessionIO implements UserSessionIO {
	private static final String USER_SESSION_JSON_FILE_NAME = "userSession.json";

	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public UserSession getPreviousUserSession() throws PokemonIOException {
		try {
			return mapper.readValue(new File(USER_SESSION_JSON_FILE_NAME), UserSession.class);
		} catch (IOException e) {
			throw new PokemonIOException(
					":( No previous user session or issues while reading the previous user state; initializing a new user session", e);
		}
	}

	@Override
	public void saveUserSession(UserSession userSession) throws PokemonIOException {
		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(new File(USER_SESSION_JSON_FILE_NAME), userSession);
		} catch (IOException e) {
			throw new PokemonIOException("Could not save the user session", e);
		}
	}

}
