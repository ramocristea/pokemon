package pokemon.io.impl;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import pokemon.io.TriviaIO;
import pokemon.model.Question;
import pokemon.model.exception.PokemonIOException;

public class FileTriviaIO implements TriviaIO {
	private static final String QUESTIONS_JSON_FILE_NAME = "questions.json";

	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public Question[] fetchQuestions() throws PokemonIOException {
		try {
			return mapper.readValue(new File(QUESTIONS_JSON_FILE_NAME), Question[].class);
		} catch (IOException ioException) {
			throw new PokemonIOException(
					"Could not read the questions from file :(; fileName=" + QUESTIONS_JSON_FILE_NAME, ioException);
		}
	}

}
