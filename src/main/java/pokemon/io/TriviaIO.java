package pokemon.io;

import pokemon.model.Question;
import pokemon.model.exception.PokemonIOException;

public interface TriviaIO {
	
	public Question[] fetchQuestions() throws PokemonIOException;

}
