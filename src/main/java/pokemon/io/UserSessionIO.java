package pokemon.io;

import pokemon.model.UserSession;
import pokemon.model.exception.PokemonIOException;

public interface UserSessionIO {
	public UserSession getPreviousUserSession() throws PokemonIOException;
	public void saveUserSession(UserSession userSession) throws PokemonIOException;
}
