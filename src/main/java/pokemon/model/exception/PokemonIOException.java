package pokemon.model.exception;

public class PokemonIOException extends Exception {

	private static final long serialVersionUID = 1L;

	public PokemonIOException() {
		super();
	}

	public PokemonIOException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PokemonIOException(String message, Throwable cause) {
		super(message, cause);
	}

	public PokemonIOException(String message) {
		super(message);
	}

	public PokemonIOException(Throwable cause) {
		super(cause);
	}

}
