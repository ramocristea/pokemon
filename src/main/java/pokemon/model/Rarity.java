package pokemon.model;

public enum Rarity {

	COMMON(70), RARE(50), MAGIC(30), EPIC(15), LEGENDARY(5);

	private int catchingChance;

	private Rarity(int catchingChance) {
		this.catchingChance = catchingChance;
	}

	public int getCatchingChance() {
		return catchingChance;
	}
}
