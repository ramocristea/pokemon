package pokemon.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("firePokemon")
public class FirePokemon extends Pokemon {
	
	public FirePokemon(){
		super();
	}
	
	/**
	 * 
	 * @param name
	 * @param colour
	 * @param rarity defined by instance, not type
	 */
	public FirePokemon(String name, String colour, Rarity rarity) {
		super(name, colour, rarity);
	}
	
	public FirePokemon(String name, String colour) {
		super(name, colour);
	}

	@Override
	public String runAway() {
		return getName() + " burst into flames and disappeared :(";
	}
}
