package pokemon.model;

import java.util.ArrayList;
import java.util.List;

public class UserSession {
	
	 private List<Pokemon> catchedPokemons = new ArrayList<>();
	 
	 private int numberOfPokeBalls = 10;

	public List<Pokemon> getCatchedPokemons() {
		return catchedPokemons;
	}

	public int getNumberOfPokeBalls() {
		return numberOfPokeBalls;
	}
	
	public void substractPokeBall() {
		numberOfPokeBalls -= 1;
	}
	
	public void addPokeBalls(int amount) {
		numberOfPokeBalls += amount;
	}
	 
}
