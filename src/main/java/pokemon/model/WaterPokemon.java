package pokemon.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("waterPokemon")
public class WaterPokemon extends Pokemon {
	
	public WaterPokemon(){
		super();
	}

	/**
	 *
	 * @param name
	 * @param colour
	 * @param rarity
	 *            defined by instance, not type
	 */
	public WaterPokemon(String name, String colour, Rarity rarity) {
		super(name, colour, rarity);
	}

	public WaterPokemon(String name, String colour) {
		super(name, colour);
	}
	
	@Override
	public String runAway() {
		return getName() + " swam away and disappeared :(";
	}
}
