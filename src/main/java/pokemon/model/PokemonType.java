package pokemon.model;

/**
 * we will not use it as a type
 * @author cristear
 *
 */
public enum PokemonType {
	BUG, DRAGON, ELECTRIC, FAIRY, FIRE, FLYING, GHOST, GRASS, GROUND, NORMAL, POISON, PSYCHIC, ROCK, WATER;   
}
