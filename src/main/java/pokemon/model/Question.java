package pokemon.model;

public class Question {

	public String question;
	public String answer;
	public int pokeBallsReward;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getPokeBallsReward() {
		return pokeBallsReward;
	}

	public void setPokeBallsReward(int pokeBallsReward) {
		this.pokeBallsReward = pokeBallsReward;
	}

}
