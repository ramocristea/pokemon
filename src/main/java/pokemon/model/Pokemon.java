package pokemon.model;

import java.util.Random;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import pokemon.action.Catchable;
import pokemon.util.PokemonUtils;

/* Polymophic deserialization */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = FirePokemon.class, name = "firePokemon"),
		@Type(value = WaterPokemon.class, name = "waterPokemon") })

public abstract class Pokemon implements Catchable {

	private String name;

	private String colour;

	private Rarity rarity = Rarity.COMMON;
	
	public Pokemon(){};

	public Pokemon(String name, String colour) {
		this.name = name;
		this.colour = colour;
	}

	public Pokemon(String name, String colour, Rarity rarity) {
		this(name, colour);
		this.rarity = rarity;
	}

	public abstract String runAway();

	public int catchMe() {
		return rarity.getCatchingChance();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	@JsonIgnore
	public boolean isCaptured() {
		int chance = new Random().nextInt(PokemonUtils.MAX);
		return chance < catchMe();
	}

	@Override
	public String toString() {
		return "a " + colour + " " + name;
	}
}
